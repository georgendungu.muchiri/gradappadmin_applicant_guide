.. image:: logo1.jpg
	:align: center


Welcome to GradApp Applicant User Guide!
========================================


GradApp platform allows one to apply for masters on the portal.One first views the Masters programs available on offer,then an interested applicant registers to get acces  to the portal,uploads all the required documents e.g CV,academic certificates,professional certificates
.After uplaoding the certificates and making the application for the interested program,one needs to wait for a approval if they are eligble to apply for that program so that they can proceed to make payment for the entrance exam.

Contents:
=========

.. toctree::
   :maxdepth: 1
   
   Getting Started
   Login Process
   Main Portal
   Profile Creation
   Program Application Process
   Application Fee Payment



Links:
======

* **Admin Portal**
	* `GradApp <http://gradapp.strathmore.edu/>`_
