Profile Creation
================

First step after account creation should be (preferably) profile creation; though this can be done at whatever stage. The page is accessible through the Profile button on the navigation drawer and  has content grouped under the following categories:

#. Profile Creation
#. First step after account creation should be (preferably) profile creation; though this can be done at whatever stage. The page is accessible through the Profile button on the navigation drawer and  has content grouped under the following categories:
#. Basic Information
#. Contact Information
#. Academic Qualifications
#. Professional Qualification 
#. Guardian/Spouse/Relative Details (Limited to 2 entries) 
#. Referee Details (Limited to 3 entries) 
#. Work Experience (Limited to 3 entries)

.. image:: profile_creation.png
  :align: center

Basic Information
*****************

.. image:: basic_information.png
  :align: center


This category includes the user’s nationality, date of birth, gender, marital status & religion. The user’s nationality, date of birth and gender are already collected during account creation, therefore cannot be updated. His/her marital status and religion can however be updated by clicking on the button next to the user’s name that is represented as a ‘pen’ icon.

This action should trigger a modal to pop up and allow the user to pick an option from the pre-populated drop down list.


.. image:: basic_data.png
  :align: center

Once done, the save button can be used to update the basic data.

Users can also add an image to their profiles by clicking on the Change Image button. This should trigger a modal with a file picker input field to appear. Clicking on the input field will open the user’s file explorer where an image is expected as the input. Once the image has been selected, the user should hit the submit button in order to upload it to the server. The profile image should then be updated to reflect the change.


Contact Information
*******************

Contact Information includes email (uneditable), phone number, post office box number, home district, permanent address and physical address in Nairobi. These fields can be edited by clicking on the button with a ‘pen’ icon, which should activate a modal that holds a form with all the related input fields.

.. image:: contact_add.png
  :align: center
