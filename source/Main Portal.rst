Main Portal
===========
Once the user has successfully authenticated they are redirected into the portal’s homepage

.. image:: main_portal.png
  :align: center


The portal features five navigation buttons on the side bar (navigation drawer) which are used to access the various pages on the system; and a sign out button. The default view is the home page which features a summary of total events on the system, total applications and uploads. It also features a calendar which marks the date that the previously mentioned events occurred along with the opportunity for applicants to trigger certain events depending on the stage they are at in their various applications.

    #.  System Events include;
    #.  Course application date & time
    #.  Document upload date, time & type
    #.  Transcript upload date, time & type
    #.  Reference receipt date & time
    #.  Application fee payment date, time & reference code
    #.  Interview date, time and link
    #.  Exam date & time
