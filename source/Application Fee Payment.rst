Application Fee Payment Process
===============================
Once the application is received by the relevant admin they review the application and they can either reject or give a greenlight that the applicant is eligible to do the program.If an applicant is eligible they will receive a notification on the platform and also on email asking them to proceed with the application fee payment.An applicant should be able to make a secure online payment using iLab Pay payment platform that has been integrated if they have been approved.The payment options involves either of this two methods.

#. Enter Phone No (For Mpesa Payment)
#. Enter Card Details (For Debit/Credit Card Payment)
