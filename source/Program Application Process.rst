Program Application Process
===========================
After a successful login the applicant proceeds and makes an application.To make a successful application the following details must be submitted:
(All required field have an asterisk *)

    #. Choose Faculty
    #. Choose Program
    #. Choose Study Mode
    #. Enter Fee Payment Details
    #. Confirm if applicant has any criminal activity convictions

After a successful application ,an applicant can review and make any necessary changes on the application.
