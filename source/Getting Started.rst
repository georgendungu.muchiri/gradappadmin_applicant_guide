Getting Started
===============

Site is located at http://gradapp.strathmore.edu/
The home page features an overview of the web app along with valuable information on how the site works and available courses

Login Process 
=============

Both the account creation and login processes share a common modal (pop-up) which can be accessed by clicking on the Sign in button on the main menu’s header (Desktop View) or the Sign in button on the navigation drawer (Mobile view)
Once the button is clicked, a modal appears as shown below. By default, the modal shows the login screen where users can access their accounts by simply inputting their email and password and clicking on the login button.

.. image:: student-login.png
  :align: center

However, new users who do not have an account and wish to create one can do so by clicking on the ‘Create an account’ button below the Login button. This should bring up the registration form. The form has several fields such as First and Last Name fields which are required in order to create an account, if left blank, these fields will be highlighted in red when the user tries to proceed and create an account. These required fields can be identified by an asterisk (*) which applies to all the forms on the application.

Another point that should be highlighted is in relation to the password foo=ield which requires a minimum of 8 characters, at least one should be capitalized, a number and a special character.

.. image:: applicant-registration.png
  :align: center

Once the form has been correctly filled, the user can hit the create button to submit the data to the system. If the email submitted does exist, the system will generate an email that has a verification code and send it to the provided email address.

.. image:: account-activate.png
  :align: center

Click on the link within the email to activate your account and gain access to the portal.


.. image:: email-activation.png
    :align: center